# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

 clone this rep: git clone https://kanstantsin_papkou@bitbucket.org/kanstantsin_papkou/soap-wk-train.git

   go to web-service folded:

     mvn package
  
     mvn tomcat7:run-war
 
     WSDL: http://localhost:8080/web-services/soap?wsdl

   go to soap-client package:

     mvn pacakge

     cd target

### How to run client and set params? ### !!!!!!!!!!!!!!!!!!!
     java -jar soap-client-0.0.1-SNAPSHOT.jar {main person birth date(dd-MM-yyyy)} {friends birth dates... (dd-MM-yyyy)} {compare birth date(dd-MM-yyyy)},

 matches will found by year of birth

### Example ###

     java -jar soap-client-0.0.1-SNAPSHOT.jar 12-12-1995 15-10-1994 14-11-1998 14-11-1995 14-12-1995 11-10-1995

  output:

    Col of friends with 1995 year of birth:
    2