package com.epam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.epam.services.NoFriendsByYearException_Exception;
import com.epam.services.Person;
import com.epam.services.SoapService;
import com.epam.services.SoapService_Service;

public class Main {
	public static void main(String[] args) {

		SoapService_Service impl = new SoapService_Service();
		SoapService ss = impl.getSoapServiceImplPort();
		
		Person mainPerson;
		
		try {
			
			mainPerson = personFactory("man", args[0]);
			
			for (int i = 1; i < args.length - 1; i++) {
				mainPerson.getFriends().add(personFactory("Friend" + i, args[i]));
			}
			
			XMLGregorianCalendar compareYear = parseDate(args[args.length-1]);
			List<Person> persons = ss.getFriends(mainPerson, compareYear);
			
			System.out.println("Col of friends with " + compareYear.getYear() + " year of birth:");
			System.out.println(persons.size());
			

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		} catch (NoFriendsByYearException_Exception e) {
			System.err.println(e.getMessage());
		}

	}
	
	
	private static Person personFactory(String name, String date) throws ParseException, DatatypeConfigurationException{
		Person person = new Person();

		person.setName(name);
		person.setBirthday(parseDate(date));
		
		return person;
	}
	
	private static XMLGregorianCalendar parseDate(String date) throws DatatypeConfigurationException, ParseException{
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(dateFormat.parse(date));
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		return date2;
	}
}
