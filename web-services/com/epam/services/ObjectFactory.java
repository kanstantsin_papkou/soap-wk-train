
package com.epam.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Person_QNAME = new QName("http://services.epam.com/", "Person");
    private final static QName _NoFriendsByYearException_QNAME = new QName("http://services.epam.com/", "NoFriendsByYearException");
    private final static QName _GetFriendsResponse_QNAME = new QName("http://services.epam.com/", "getFriendsResponse");
    private final static QName _GetFriends_QNAME = new QName("http://services.epam.com/", "getFriends");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFriends }
     * 
     */
    public GetFriends createGetFriends() {
        return new GetFriends();
    }

    /**
     * Create an instance of {@link GetFriendsResponse }
     * 
     */
    public GetFriendsResponse createGetFriendsResponse() {
        return new GetFriendsResponse();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link NoFriendsByYearException }
     * 
     */
    public NoFriendsByYearException createNoFriendsByYearException() {
        return new NoFriendsByYearException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Person }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.epam.com/", name = "Person")
    public JAXBElement<Person> createPerson(Person value) {
        return new JAXBElement<Person>(_Person_QNAME, Person.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoFriendsByYearException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.epam.com/", name = "NoFriendsByYearException")
    public JAXBElement<NoFriendsByYearException> createNoFriendsByYearException(NoFriendsByYearException value) {
        return new JAXBElement<NoFriendsByYearException>(_NoFriendsByYearException_QNAME, NoFriendsByYearException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFriendsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.epam.com/", name = "getFriendsResponse")
    public JAXBElement<GetFriendsResponse> createGetFriendsResponse(GetFriendsResponse value) {
        return new JAXBElement<GetFriendsResponse>(_GetFriendsResponse_QNAME, GetFriendsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFriends }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.epam.com/", name = "getFriends")
    public JAXBElement<GetFriends> createGetFriends(GetFriends value) {
        return new JAXBElement<GetFriends>(_GetFriends_QNAME, GetFriends.class, null, value);
    }

}
