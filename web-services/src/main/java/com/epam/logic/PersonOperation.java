package com.epam.logic;

import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.epam.beans.Person;
import com.epam.exceptions.NoFriendsByYearException;

public class PersonOperation {
	
	public List<Person> getFriendsByYear(Person person, Date date) throws NoFriendsByYearException{
		List<Person> friends = new ArrayList<Person>();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int compareYear = calendar.get(Calendar.YEAR);
		
		for (Person friend : person.getFriends()) {
			calendar.setTime(friend.getBirthday());
			if (compareYear == calendar.get(Calendar.YEAR)) {
				friends.add(friend);
			}
		}
		
		if (friends.isEmpty()) {
			throw new NoFriendsByYearException("No friends by " + compareYear);
		}
		
		return friends;
	}
}
