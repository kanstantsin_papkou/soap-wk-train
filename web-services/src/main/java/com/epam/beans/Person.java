package com.epam.beans;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Person")
public class Person {
	private String name;
	private Date birthday;
	private List<Person> friends;
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Person(String name, Date birthday, List<Person> friends) {
		super();
		this.name = name;
		this.birthday = birthday;
		this.friends = friends;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public List<Person> getFriends() {
		return friends;
	}
	public void setFriends(List<Person> friends) {
		this.friends = friends;
	}
	
}
