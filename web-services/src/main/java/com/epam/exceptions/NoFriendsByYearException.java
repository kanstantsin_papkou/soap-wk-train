package com.epam.exceptions;

import javax.xml.ws.WebFault;

@WebFault
public class NoFriendsByYearException extends Exception {

	private static final long serialVersionUID = -6267123035728983609L;

	public NoFriendsByYearException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoFriendsByYearException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoFriendsByYearException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoFriendsByYearException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
