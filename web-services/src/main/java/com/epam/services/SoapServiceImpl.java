package com.epam.services;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.core.NewCookie;

import com.epam.beans.Person;
import com.epam.exceptions.NoFriendsByYearException;
import com.epam.logic.PersonOperation;

@WebService(endpointInterface = "com.epam.services.SoapService", serviceName = "SoapService")
public class SoapServiceImpl implements SoapService {
	
	private PersonOperation personOperation;

	public SoapServiceImpl() {
		super();
		this.personOperation = new PersonOperation();
	}

	@Override
	public List<Person> getFriends(Person person, Date date) throws NoFriendsByYearException {
		return  personOperation.getFriendsByYear(person, date);
	}
}
