package com.epam.services;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.epam.beans.Person;
import com.epam.exceptions.NoFriendsByYearException;

@WebService
public interface SoapService {

	@WebMethod
	public List<Person> getFriends(Person person, Date date) throws NoFriendsByYearException;
}
