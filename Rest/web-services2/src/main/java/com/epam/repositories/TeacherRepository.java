package com.epam.repositories;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.xml.resolver.apps.resolver;
import org.aspectj.apache.bcel.generic.NEW;

import com.epam.beans.Lesson;
import com.epam.beans.Teacher;
import com.epam.beans.exceptions.NotModifiedException;

public class TeacherRepository {

	private static Map<Long, Teacher> teachers = new TreeMap<>();

	static {
		Lesson lesson1 = new Lesson(1, "math", 55);
		Lesson lesson2 = new Lesson(2, "english", 49);

		List<Lesson> lessons = new ArrayList<>();
		List<Lesson> lessons2 = new ArrayList<>();
		{
			lessons.add(lesson1);
			lessons.add(lesson2);
			lessons2.add(lesson1);
		}
		Teacher teacher1 = new Teacher(1, "Aleksey", new Date(1111111111), lessons);
		Teacher teacher2 = new Teacher(2, "Konstantin", new Date(new java.util.Date().getTime()), lessons2);

		teachers.put(teacher1.getId(), teacher1);
		teachers.put(teacher2.getId(), teacher2);
	}

	public Teacher getTeacherById(long id) {
		return teachers.get(id);
	}

	public void updateTeacher(Teacher newTeacher) throws NotModifiedException {
		long id = newTeacher.getId();
		Teacher oldTeacher = teachers.get(id);

		if (newTeacher.equals(oldTeacher)) {
			throw new NotModifiedException();
		}

		teachers.put(id, newTeacher);
	}

	public void deleteTeacher(long id) {
		teachers.remove(id);
	}

	public void addTeacher(Teacher newTeacher) {
		teachers.put(newTeacher.getId(), newTeacher);
	}

	public List<Teacher> getAllTEachers() {
		return new ArrayList<Teacher>(teachers.values());
	}

	public Teacher getBusiestTeacher(){
        Teacher busiestTeacher = getAllTEachers().get(0);
		
		for (Teacher teacher : getAllTEachers()) {
			if (teacher.getLessons().size() > busiestTeacher.getLessons().size()) {
				busiestTeacher = teacher;
			}
		}
		return busiestTeacher;

	}
}
