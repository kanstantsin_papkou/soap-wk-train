package com.epam.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.epam.beans.Teacher;
import com.epam.beans.exceptions.NotModifiedException;

public interface RestService {

	@GET
	@Path("/{id}")
	@Produces("application/json")
	@Consumes("application/xml")
	public Teacher getTeacher(@PathParam("id") long id);
	
	@GET
	@Path("/busi")
	@Produces("application/json")
	@Consumes("application/xml")
	public Teacher getBusiestTeacher();

	@GET
	@Path("/")
	@Produces("application/json")
	@Consumes("application/xml")
	public List<Teacher> getAllTeachers();

	@PUT
	@Path("/")
	@Consumes("application/xml")
	public Response updateTeacher(Teacher teacher) throws NotModifiedException;

	@DELETE
	@Path("/{id}")
	@Produces("application/json")
	@Consumes("application/xml")
	public void deleteTeacher(@PathParam("id") long id);

	@POST
	@Path("/")
	@Consumes("application/xml")
	public void addTeacher(Teacher teacher);
}
