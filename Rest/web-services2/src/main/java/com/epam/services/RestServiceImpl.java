package com.epam.services;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.epam.beans.Teacher;
import com.epam.beans.exceptions.NotModifiedException;
import com.epam.repositories.TeacherRepository;

public class RestServiceImpl implements RestService {
	
	private TeacherRepository teacherRepository;

	public RestServiceImpl() {
		super();
		this.teacherRepository = new TeacherRepository();
	}

	@Override
	public Teacher getBusiestTeacher() {
		return teacherRepository.getBusiestTeacher();
	}

	@Override
	public Teacher getTeacher(long id) {
		return teacherRepository.getTeacherById(id);
	}

	@Override
	public List<Teacher> getAllTeachers() {
		return teacherRepository.getAllTEachers();
	}

	@Override
	public Response updateTeacher(Teacher teacher) throws NotModifiedException {
		try {
			teacherRepository.updateTeacher(teacher);
			return Response.ok(teacher, MediaType.APPLICATION_JSON).build();
		} catch (NotModifiedException e) {
			return Response.status(Response.Status.NOT_MODIFIED).entity("This teacher not modified").build();
		}

	}

	@Override
	public void deleteTeacher(long id) {
		teacherRepository.deleteTeacher(id);
	}

	@Override
	public void addTeacher(Teacher teacher) {
		teacherRepository.addTeacher(teacher);
	}

}
