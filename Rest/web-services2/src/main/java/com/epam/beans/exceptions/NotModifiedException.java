package com.epam.beans.exceptions;

public class NotModifiedException extends Exception{

	public NotModifiedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotModifiedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotModifiedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotModifiedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
