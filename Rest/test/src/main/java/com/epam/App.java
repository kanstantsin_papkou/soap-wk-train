package com.epam;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.epam.beans.Lesson;
import com.epam.beans.Teacher;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		WebTarget pathBase = client.target("http://localhost:8080/web-services/teachers/");
		List<Teacher> teachers = new ArrayList<Teacher>();

		Teacher teacher = pathBase.path("1").request(MediaType.APPLICATION_JSON).get(Teacher.class);
		System.out.println("get teacher id = 1 : " + teacher);

		teacher.setId(3);
		teacher.setName("Avgust");
		System.out.println("create teacher with id =3");
		pathBase.request(MediaType.APPLICATION_XML).post(Entity.xml(teacher), Teacher.class);

		teacher = pathBase.path("3").request(MediaType.APPLICATION_JSON).get(Teacher.class);
		System.out.println("get teacher id = 3, name=august : " + teacher);

		System.out.println("update teachers name  with id =3");
		teacher.setName("Peter");
		pathBase.request(MediaType.APPLICATION_XML).put(Entity.xml(teacher), Teacher.class);

		teacher = pathBase.path("3").request(MediaType.APPLICATION_JSON).get(Teacher.class);
		System.out.println("get teacher id = 3, name=peter : " + teacher);

		System.out.println("delete teacher  with id =3");
		pathBase.path("3").request().delete();
		
		teacher = pathBase.path("busi").request(MediaType.APPLICATION_JSON).get(Teacher.class);
		System.out.println("busiest teacher : " + teacher);

		Lesson lesson1 = new Lesson(1, "math", 55);
		Lesson lesson2 = new Lesson(2, "english", 49);

		List<Lesson> lessons = new ArrayList<Lesson>();
		{
			lessons.add(lesson1);
			lessons.add(lesson2);
		}
		Teacher teacher1 = new Teacher(1, "Aleksey", new Date(1111111111), lessons);

		System.out.println("get teacher id = 3 should be - not modified 304 : ");
		pathBase.request(MediaType.APPLICATION_XML).put(Entity.xml(teacher1), Teacher.class);
		// teacher =
		// pathBase.path("3").request(MediaType.APPLICATION_JSON).get(Teacher.class);
	}
}
